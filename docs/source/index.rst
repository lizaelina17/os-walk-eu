OS-WALK-EU Documentation
========================

.. note::
   This is experimental work

About this Plugin
-----------------

.. todo::
   Add background infos.

Table of Contents
-----------------

.. toctree::
   :maxdepth: 3
   
   WalkabilityAlgorithm.rst

.. toctree::
   :maxdepth: 1

   install.rst
   gitlog
   todo
   
Index and search
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

