Inline-Documentation of WalkieProcessingAlgorithm
=================================================

.. note::
   Version |release|

.. class:: WalkieProcessingAlgorithm

.. automodule:: walkability_plugin.algorithm.walkability_algorithm.WalkieProcessingAlgorithm
   :members:
   :private-members:
   :special-members: __init__
   :undoc-members: