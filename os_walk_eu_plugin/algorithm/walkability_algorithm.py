# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

import os
import numpy as np
import shutil
import json
import pandas as pd
import ast

from qgis.utils import iface
from qgis.core import QgsMessageLog


from ..gui.walkability_plugin_dialog import WalkabilityToolDialog

from qgis.core import (QgsVectorLayer,
                       QgsCoordinateReferenceSystem,
                       QgsProcessingAlgorithm)
import processing
from osgeo import ogr


class WalkieProcessingAlgorithm(QgsProcessingAlgorithm):
    """
    This is a walkability algorithm
    """

    def __init__(self, gridLayer, maxDist, elevation, output
                 , amenities_weight, pedestrian_shed_weight,  green_area_weight
                 , supermarket_count, education_count, shopping_count, other_errands_count, leisure_count
                 , list_supermarket_weights, list_education_weights, list_shopping_weights
                 , list_other_errands_weights, list_leisure_weights,expertMode,profile1,profile2,supermarket_dataset
                 , education_dataset,shopping_dataset,errands_dataset, leisure_dataset,supermarketLayer,educationLayer
                 ,shoppingLayer,errandsLayer,leisureLayer,elevation_checkbox):
        super().__init__()
        self.gridLayer = gridLayer
        self.elevation = elevation
        self.output = output
        self.supermarketLayer = supermarketLayer
        self.educationLayer = educationLayer
        self.shoppingLayer = shoppingLayer
        self.errandsLayer = errandsLayer
        self.leisureLayer = leisureLayer
        global supermarket_dataset_check
        global education_dataset_check
        global shopping_dataset_check
        global errands_dataset_check
        global leisure_dataset_check
        global elevation_global
        supermarket_dataset_check= supermarket_dataset
        education_dataset_check = education_dataset
        shopping_dataset_check = shopping_dataset
        errands_dataset_check = errands_dataset
        leisure_dataset_check = leisure_dataset
        elevation_global=elevation_checkbox


        if expertMode == True:
            #Define maxDistances in GUI
            self.maxDist = maxDist

            # Weightings for the calculation of the final score
            self.AMENITIES_WEIGHT = amenities_weight
            self.PEDESTRIAN_SHED_WEIGHT = pedestrian_shed_weight
            self.GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT = green_area_weight

            # Defining number of amenities to search for in the maximum walking distance
            self.SUPERMARKET_COUNT = supermarket_count
            self.EDUCATION_COUNT = education_count
            self.SHOPPING_COUNT = shopping_count
            self.OTHER_ERRANDS_COUNT = other_errands_count
            self.LEISURE_COUNT = leisure_count

            # Define weightings for amenities where more than one are searched for
            self.AMENITY_SUPERMARKET_WEIGHT_1 = list_supermarket_weights[0]
            self.AMENITY_SUPERMARKET_WEIGHT_2 = list_supermarket_weights[1]
            self.AMENITY_SUPERMARKET_WEIGHT_3 = list_supermarket_weights[2]
            self.AMENITY_EDUCATION_WEIGHT_1 = list_education_weights[0]
            self.AMENITY_EDUCATION_WEIGHT_2 = list_education_weights[1]
            self.AMENITY_EDUCATION_WEIGHT_3 = list_education_weights[2]
            self.AMENITY_SHOPPING_WEIGHT_1 = list_shopping_weights[0]
            self.AMENITY_SHOPPING_WEIGHT_2 = list_shopping_weights[1]
            self.AMENITY_SHOPPING_WEIGHT_3 = list_shopping_weights[2]
            self.AMENITY_OTHER_ERRANDS_WEIGHT_1 = list_other_errands_weights[0]
            self.AMENITY_OTHER_ERRANDS_WEIGHT_2 = list_other_errands_weights[1]
            self.AMENITY_OTHER_ERRANDS_WEIGHT_3 = list_other_errands_weights[2]
            self.AMENITY_LEISURE_WEIGHT_1 = list_leisure_weights[0]
            self.AMENITY_LEISURE_WEIGHT_2 = list_leisure_weights[1]
            self.AMENITY_LEISURE_WEIGHT_3 = list_leisure_weights[2]

        else:
            iface.messageBar().pushMessage(str(profile1), str(profile2),"check")
            Profile = "P" + str(profile1) + str(profile2)
            profileList = ['P00','P01','P02','P03','P10','P20','P30','P40','P10','P11','P12','P13','P20','P21','P22','P23','P30','P31','P32','P33','P40','P41','P42','P43']
            index = profileList.index(Profile)
            vals =[
                [1, 0.4, 0.6, 1, 2, 2, 3, 2, 4, 0, 0, 2, 1, 0, 2, 1, 0, 2.5, 1, 1, 2.5, 1.5, 0,500],#P00
                [1, 0.4, 0.6, 1, 2, 2, 3, 2, 4.5, 0, 0, 2, 1.5, 0, 2, 1, 0, 3, 1, 1, 2.5, 1, 0,500],#P01
                [1, 0.4, 0.6, 1, 2, 2, 3, 3, 9, 0, 0, 2, 1.5, 0, 4, 2, 0, 5, 3, 2, 4, 2.5, 1.5,500],#P02
                [1, 0.4, 0.6, 1, 2, 2, 3, 3, 7, 0, 0, 5, 3, 0, 4, 2, 0, 4, 2, 1.5, 4, 3, 1.5,500],#P03
                [1, 0.4, 0.6, 1, 2, 1, 1, 2, 1, 0, 0, 4.5, 2.5, 0, 1.5, 0, 0, 1.5, 0, 0, 3, 1, 0,500],#P10
                [1, 0.2, 0.6, 1, 2, 2, 2, 3, 3, 0, 0, 3, 1, 0, 2, 1, 0, 2.5, 1.5, 0, 2, 1.5, 1,700],#P20
                [1, 0.3, 0.6, 1, 0, 2, 3, 2, 5, 0, 0, 0, 0, 0, 2, 1.5, 0, 3, 1.5, 1, 3, 1, 0,600],#P30
                [1, 0.5, 0.6, 1, 0, 2, 3, 2, 5, 0, 0, 0, 0, 0, 2, 1, 0, 3.5, 2, 1, 2.5, 1, 0,400],#P40
                [1, 0.4, 0.6, 1, 2, 1, 1, 2, 1, 0, 0, 7, 3, 0, 1.5, 0, 0, 1.5, 0, 0, 2.5, 1, 0,500],#P11
                [1, 0.4, 0.6, 1, 2, 1, 1, 2, 1, 0, 0, 7, 3, 0, 1, 0, 0, 1.5, 0, 0, 3, 1, 0,500],#P12
                [1, 0.4, 0.6, 1, 2, 1, 1, 2, 1, 0, 0, 7, 3, 0, 1.5, 0, 0, 1, 0, 0, 3, 1, 0,500],#P13
                [1, 0.2, 0.6, 1, 2, 2, 2, 2, 3.5, 0, 0, 3.5, 2, 0, 1.5, 1, 0, 2.5, 1.5, 0, 3, 1, 0,700],#P21
                [1, 0.2, 0.6, 1, 2, 2, 2, 3, 4, 0, 0, 2, 1.5, 0, 2, 1, 0, 2.5, 1.5, 0, 2, 1.5, 1,700],#P22
                [1, 0.2, 0.6, 1, 2, 2, 2, 3, 2.5, 0, 0, 3, 1.5, 0, 1.5, 1, 0, 2.5, 1.5, 0, 2, 1.5, 1,700],#P23
                [1, 0.3, 0.6, 1, 0, 2, 3, 2, 6, 0, 0, 0, 0, 0, 3, 1, 0, 3, 2, 1, 2.5, 1, 0,600],#P31
                [1, 0.3, 0.6, 1, 0, 2, 3, 2, 5, 0, 0, 0, 0, 0, 2, 1.5, 0, 3, 1.5, 1, 3, 1, 0,600],#P32
                [1, 0.3, 0.6, 1, 0, 2, 3, 3, 5, 0, 0, 0, 0, 0, 3, 1, 0, 3, 1, 1, 2, 1.5, 1,600],#P33
                [1, 0.5, 0.6, 1, 0, 2, 3, 2, 5.5, 0, 0, 0, 0, 0, 2, 1, 0, 3.5, 2, 1, 2.5, 1, 0,400],#P41
                [1, 0.5, 0.6, 1, 0, 2, 3, 2, 5, 0, 0, 0, 0, 0, 2, 1, 0, 3.5, 2, 1, 2.5, 1, 0,400],#P42
                [1, 0.5, 0.6, 1, 0, 2, 3, 2, 5.5, 0, 0, 0, 0, 0, 2, 1, 0, 3, 2, 1, 3, 1, 0,400]#P42
            ]

            # Weightings for the calculation of the final score
            self.AMENITIES_WEIGHT = vals[index][0]
            self.PEDESTRIAN_SHED_WEIGHT = vals[index][1]
            self.GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT = vals[index][2]

            # Defining number of amenities to search for in the maximum walking distance
            self.SUPERMARKET_COUNT = vals[index][3]
            self.EDUCATION_COUNT = vals[index][4]
            self.SHOPPING_COUNT = vals[index][5]
            self.OTHER_ERRANDS_COUNT = vals[index][6]
            self.LEISURE_COUNT = vals[index][7]

            # Define weightings for amenities where more than one are searched for
            self.AMENITY_SUPERMARKET_WEIGHT_1 = vals[index][8]
            self.AMENITY_SUPERMARKET_WEIGHT_2 = vals[index][9]
            self.AMENITY_SUPERMARKET_WEIGHT_3 = vals[index][10]
            self.AMENITY_EDUCATION_WEIGHT_1 = vals[index][11]
            self.AMENITY_EDUCATION_WEIGHT_2 = vals[index][12]
            self.AMENITY_EDUCATION_WEIGHT_3 = vals[index][13]
            self.AMENITY_SHOPPING_WEIGHT_1 = vals[index][14]
            self.AMENITY_SHOPPING_WEIGHT_2 = vals[index][15]
            self.AMENITY_SHOPPING_WEIGHT_3 = vals[index][16]
            self.AMENITY_OTHER_ERRANDS_WEIGHT_1 = vals[index][17]
            self.AMENITY_OTHER_ERRANDS_WEIGHT_2 = vals[index][18]
            self.AMENITY_OTHER_ERRANDS_WEIGHT_3 = vals[index][19]
            self.AMENITY_LEISURE_WEIGHT_1 = vals[index][20]
            self.AMENITY_LEISURE_WEIGHT_2 = vals[index][21]
            self.AMENITY_LEISURE_WEIGHT_3 = vals[index][22]

            self.maxDist = vals[index][23]
    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.
    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'

    DIST_FIELD = 'DIST_KM'  # ORS distance field in matrix

    def walkability_algorithm(self):
        """
        Here is where the processing itself takes place.
        """

        try:
            os.remove(self.output + r'/tempDir')
        except OSError:
            pass

        try:
            os.mkdir(self.output + r'/tempDir')
        except:
            pass

        try:
            poiLayer =[self.supermarketLayer,self.educationLayer,self.shoppingLayer,self.errandsLayer,self.leisureLayer]
            poiName =["SU","ED","SH","OE","LE"]
            c=0
            for layer in poiLayer:
                processing.run("gdal:convertformat", {'INPUT': layer, 'OPTIONS': '',
                                                          'OUTPUT': self.output + r'\\tempDir\\'+poiName[c]+'.geojson'})
                c+=1
            # Add an incremental ID to the layer for later processing.algorithmHelp("native:addautoincrementalfield")
        except:
            pass
        autoIncrement = processing.run("native:addautoincrementalfield",
                                       {
                                           'INPUT': self.gridLayer,
                                           'FIELD_NAME': 'Walk_ID',
                                           'START': 1,
                                           'GROUP_FIELDS': [],
                                           'SORT_EXPRESSION': '',
                                           'SORT_ASCENDING': True,
                                           'SORT_NULLS_FIRST': False,
                                           'OUTPUT': 'memory:Grid_500'
                                       }
                                       )['OUTPUT']

        autoIncrementproj = processing.run("native:assignprojection",
                                           {'INPUT': autoIncrement,
                                            'CRS': QgsCoordinateReferenceSystem('EPSG:3035'),
                                            'OUTPUT': 'memory:increment'
                                            }
                                           )['OUTPUT']


        # Extract centroids for cells for routing purposes
        polyCentroids = processing.run("native:centroids",
                                       {
                                           'INPUT': autoIncrement,
                                           'ALL_PARTS': False,
                                           'OUTPUT': self.output + r'/tempDir/polycentroids.geojson'
                                       }
                                       )['OUTPUT']

        # Create a search buffer of 3000 meters as initial mask for the amenity search
        buffer_layer = processing.run("native:buffer",
                                      {
                                          'INPUT': polyCentroids,
                                          'DISTANCE': self.maxDist,
                                          # check this if suitable, i.e. depends on cutoff value of used distance decay function
                                          'SEGMENTS': 100,
                                          'END_CAP_STYLE': 0,
                                          'JOIN_STYLE': 0,
                                          'MITER_LIMIT': 2,
                                          'DISSOLVE': False,
                                          'OUTPUT': 'memory:buffer'
                                      }
                                      )['OUTPUT']

        grid_geojson = processing.run("native:addautoincrementalfield",
                                      {
                                          'INPUT': self.gridLayer,
                                          'FIELD_NAME': 'Walk_ID',
                                          'START': 1,
                                          'MODULUS': 0,
                                          'GROUP_FIELDS': [],
                                          'SORT_EXPRESSION': '',
                                          'SORT_ASCENDING': True,
                                          'SORT_NULLS_FIRST': False,
                                          'OUTPUT': self.output + r'/tempDir/Grid500_ID.geojson'
                                      }
                                      )['OUTPUT']

         # Create Walking Area for the analysis
        walkarea = processing.run("ORS Tools:isochrones_from_layer",
                       {'INPUT_PROVIDER': 0,
                        'INPUT_PROFILE': 6,
                        'INPUT_POINT_LAYER': polyCentroids,
                        'INPUT_FIELD': 'Walk_ID',
                        'INPUT_METRIC': 1,
                        'INPUT_RANGES': str(self.maxDist),
                        'INPUT_AVOID_FEATURES': [],
                        'INPUT_AVOID_BORDERS': None,
                        'INPUT_AVOID_COUNTRIES': '',
                        'INPUT_AVOID_POLYGONS': None,
                        'OUTPUT': self.output + r'/tempDir/walkarea.geojson'})['OUTPUT']

        walkareaAmenities_raw = processing.run("ORS Tools:isochrones_from_layer",
                       {'INPUT_PROVIDER': 0,
                        'INPUT_PROFILE': 6,
                        'INPUT_POINT_LAYER':polyCentroids,
                        'INPUT_FIELD': 'Walk_ID',
                        'INPUT_METRIC': 1,
                        'INPUT_RANGES': '250,500,750,1000,1250,1500,1750,2000',
                        'INPUT_AVOID_FEATURES': [],
                        'INPUT_AVOID_BORDERS': None,
                        'INPUT_AVOID_COUNTRIES': '',
                        'INPUT_AVOID_POLYGONS': None,
                        'OUTPUT': 'memory:tempFile'})['OUTPUT']

        walkareaAmenities = processing.run("native:fixgeometries", {
            'INPUT':walkareaAmenities_raw,
            'OUTPUT':self.output + r'/tempDir/walkarea_amenities.geojson'})['OUTPUT']

        def checkSlope():
            """This functions calculates slope in the walking area to reduce the radius. If the elevation has no input
            in the GUI, this function is skipped."""
            if elevation_global ==True:

                processing.run("gdal:cliprasterbymasklayer",
                               {'INPUT': self.elevation,
                                'MASK': self.output + r'/tempDir/walkarea.geojson',
                                'SOURCE_CRS': None, 'TARGET_CRS': None, 'NODATA': None, 'ALPHA_BAND': False,
                                'CROP_TO_CUTLINE': True, 'KEEP_RESOLUTION': False, 'SET_RESOLUTION': False,
                                'X_RESOLUTION': None, 'Y_RESOLUTION': None, 'MULTITHREADING': False, 'OPTIONS': '',
                                'DATA_TYPE': 0, 'EXTRA': '', 'OUTPUT': self.output + r'/tempDir/elev_clip.tif'})

                processing.run("qgis:slope", {'INPUT': self.output + r'/tempDir/elev_clip.tif', 'Z_FACTOR': 1,
                                              'OUTPUT': self.output + r'/tempDir/slope.tif'})


                processing.run("qgis:zonalstatistics",{
                    'INPUT_RASTER': self.output + r'/tempDir/slope.tif',
                    'RASTER_BAND': 1,
                    'INPUT_VECTOR': self.output + r'/tempDir/walkarea.geojson',
                    'COLUMN_PREFIX': 'slope_', 'STATISTICS': [2],'OUTPUT':self.output + r'/tempDir/slope_tempFile.geojson'})

                slope_tempFile1=processing.run("native:joinattributestable",
                               {'INPUT': grid_geojson,
                                'FIELD': 'Walk_ID',
                                'INPUT_2': self.output + r'/tempDir/walkarea.geojson',
                                'FIELD_2': 'Walk_ID', 'FIELDS_TO_COPY': [], 'METHOD': 1, 'DISCARD_NONMATCHING': False,
                                'PREFIX': '', 'OUTPUT': 'memory:tempFile'})['OUTPUT']

                slope_tempFile2= processing.run("qgis:advancedpythonfieldcalculator", {'INPUT': slope_tempFile1,
                                                                      'FIELD_NAME':'Slope_red',
                                                                      'FIELD_TYPE':1,
                                                                      'FIELD_LENGTH':10,
                                                                      'FIELD_PRECISION':3,
                                                                      'GLOBAL':'def rec (input):\n if input <= 7:\n  return 0\n elif input >7 and input <=15:\n  return 2.5\n elif input >15 and input <=25:\n  return 5.0\n elif input >25 and input <=45:\n  return 7.5\n elif input >45:\n  return 10.0',
                                                                      'FORMULA':'value = rec(<slope_mean>)',
                                                                      'OUTPUT':self.output + r'/tempDir/slope_tempFile2.geojson'})['OUTPUT']


        def calculateAmenities():
            """
            calculates the index value for the amenity indicator
            output is written to intermediate output file <Grid_Complete.gpkg>
            """
            save = []
            featureCount = self.gridLayer.featureCount()
            pd.options.mode.chained_assignment = None  # default='warn'

            # fetch data from overpass-api
            URL = 'https://overpass.kumi.systems/api/interpreter?data='
            #URL = 'https://z.overpass-api.de/api/interpreter?data='
            #URL = 'https://lz4.overpass-api.de/api/interpreter?data='

            #Download OSM from Overpass API

            def downloadOsm():

                with open(self.output + r'/tempDir/walkarea_amenities.geojson', 'r') as geojson:
                    xcoords = []
                    ycoords = []
                    data = json.load(geojson)
                    for f in data['features']:
                        geom = f['geometry']
                        for coord in geom['coordinates'][0]:
                            xcoords.append(coord[0][0])
                            ycoords.append(coord[0][1])
                extent = [np.round(min(ycoords), 2), np.round(min(xcoords), 2), np.round(max(ycoords), 2),
                          np.round(max(xcoords), 2)]


                Supermarket ='SU[out:xml][timeout:100];(node["amenity"="supermarket"]('+str(extent)[1:-1]+');way["amenity"="supermarket"]('+str(extent)[1:-1]+');relation["amenity"="supermarket"]('+str(extent)[1:-1]+');  ); (._;>;); out body;'
                Education = 'ED[out:xml][timeout:100];(node["amenity"="childcare"]('+str(extent)[1:-1]+');node["amenity"="kindergarten"]('+str(extent)[1:-1]+');node["amenity"="school"]('+str(extent)[1:-1]+');node["amenity"="university"]('+str(extent)[1:-1]+');node["amenity"="college"]('+str(extent)[1:-1]+');node["amenity"="prep_school"]('+str(extent)[1:-1]+');way["amenity"="childcare"]('+str(extent)[1:-1]+');way["amenity"="kindergarten"]('+str(extent)[1:-1]+');way["amenity"="school"]('+str(extent)[1:-1]+');way["amenity"="university"]('+str(extent)[1:-1]+');way["amenity"="college"]('+str(extent)[1:-1]+');way["amenity"="prep_school"]('+str(extent)[1:-1]+');relation["amenity"="childcare"]('+str(extent)[1:-1]+');relation["amenity"="kindergarten"]('+str(extent)[1:-1]+');relation["amenity"="school"]('+str(extent)[1:-1]+');relation["amenity"="university"]('+str(extent)[1:-1]+');relation["amenity"="college"]('+str(extent)[1:-1]+');relation["amenity"="prep_school"]('+str(extent)[1:-1]+');  ); (._;>;); out body;'
                Shopping = 'SH[out:xml] [timeout:100]; (node["shop"="clothes"]('+str(extent)[1:-1]+');     node["shop"="boutique"]('+str(extent)[1:-1]+');     node["shop"="fashion"]('+str(extent)[1:-1]+');     node["shop"="shoes"]('+str(extent)[1:-1]+');     node["shop"="electronics"]('+str(extent)[1:-1]+');     node["shop"="hairdresser"]('+str(extent)[1:-1]+');     node["shop"="shoe_repair"]('+str(extent)[1:-1]+');     node["shop"="books"]('+str(extent)[1:-1]+');     way["shop"="clothes"]('+str(extent)[1:-1]+');     way["shop"="boutique"]('+str(extent)[1:-1]+');     way["shop"="fashion"]('+str(extent)[1:-1]+');     way["shop"="shoes"]('+str(extent)[1:-1]+');     way["shop"="computer"]('+str(extent)[1:-1]+');     way["shop"="electronics"]('+str(extent)[1:-1]+');     way["shop"="hairdresser"]('+str(extent)[1:-1]+');     way["shop"="shoe_repair"]('+str(extent)[1:-1]+');     way["shop"="books"]('+str(extent)[1:-1]+');     relation["shop"="clothes"]('+str(extent)[1:-1]+');     relation["shop"="boutique"]('+str(extent)[1:-1]+');     relation["shop"="fashion"]('+str(extent)[1:-1]+');     relation["shop"="shoes"]('+str(extent)[1:-1]+');     relation["shop"="electronics"]('+str(extent)[1:-1]+');     relation["shop"="hairdresser"]('+str(extent)[1:-1]+');     relation["shop"="shoe_repair"]('+str(extent)[1:-1]+');     relation["shop"="books"]('+str(extent)[1:-1]+'); ); (._;>;); out body;'
                OtherErrands='OE[out:xml][timeout:100];(node["healthcare"="hospital"]('+str(extent)[1:-1]+');node["healthcare"="clinic"]('+str(extent)[1:-1]+');node["amenity"="doctors"]('+str(extent)[1:-1]+');node["healthcare"="doctor"]('+str(extent)[1:-1]+');node["healthcare"="dentist"]('+str(extent)[1:-1]+');node["healthcare:speciality"="orthodontics"]('+str(extent)[1:-1]+');node["healthcare"="pharmacy"]('+str(extent)[1:-1]+');node["healthcare"="physiotherapist"]('+str(extent)[1:-1]+');node["amenity"="post_box"]('+str(extent)[1:-1]+');node["amenity"="post_office"]('+str(extent)[1:-1]+');node["amenity"="bank"]('+str(extent)[1:-1]+');node["amenity"="atm"]('+str(extent)[1:-1]+');node["amenity"="public_building"]('+str(extent)[1:-1]+');node["amenity"="townhall"]('+str(extent)[1:-1]+');node["amenity"="community_centre"]('+str(extent)[1:-1]+');way["healthcare"="hospital"]('+str(extent)[1:-1]+');way["healthcare"="clinic"]('+str(extent)[1:-1]+');way["amenity"="doctors"]('+str(extent)[1:-1]+');way["healthcare"="doctor"]('+str(extent)[1:-1]+');way["healthcare"="dentist"]('+str(extent)[1:-1]+');way["healthcare:speciality"="orthodontics"]('+str(extent)[1:-1]+');way["healthcare"="pharmacy"]('+str(extent)[1:-1]+');way["healthcare"="physiotherapist"]('+str(extent)[1:-1]+');way["amenity"="post_box"]('+str(extent)[1:-1]+');way["amenity"="post_office"]('+str(extent)[1:-1]+');way["amenity"="bank"]('+str(extent)[1:-1]+');way["amenity"="atm"]('+str(extent)[1:-1]+');way["amenity"="public_building"]('+str(extent)[1:-1]+');way["amenity"="townhall"]('+str(extent)[1:-1]+');way["amenity"="community_centre"]('+str(extent)[1:-1]+');relation["healthcare"="hospital"]('+str(extent)[1:-1]+');relation["healthcare"="clinic"]('+str(extent)[1:-1]+');relation["amenity"="doctors"]('+str(extent)[1:-1]+');relation["healthcare"="doctor"]('+str(extent)[1:-1]+');relation["healthcare"="dentist"]('+str(extent)[1:-1]+');relation["healthcare:speciality"="orthodontics"]('+str(extent)[1:-1]+');relation["healthcare"="pharmacy"]('+str(extent)[1:-1]+');relation["healthcare"="physiotherapist"]('+str(extent)[1:-1]+');relation["amenity"="post_box"]('+str(extent)[1:-1]+');relation["amenity"="post_office"]('+str(extent)[1:-1]+');relation["amenity"="bank"]('+str(extent)[1:-1]+');relation["amenity"="atm"]('+str(extent)[1:-1]+');relation["amenity"="public_building"]('+str(extent)[1:-1]+');relation["amenity"="townhall"]('+str(extent)[1:-1]+');relation["amenity"="community_centre"]('+str(extent)[1:-1]+'););(._;>;);out body;'
                Leisure= 'LE[out:xml] [timeout:100]; (node["amenity"="cinema"]('+str(extent)[1:-1]+');     node["tourism"="zoo"]('+str(extent)[1:-1]+');     node["leisure"="swimming_pool"]('+str(extent)[1:-1]+');     node["leisure"="swimming_area"]('+str(extent)[1:-1]+');     node["leisure"="fitness_station"]('+str(extent)[1:-1]+');     node["amenity"="public_bath"]('+str(extent)[1:-1]+');     node["leisure"="dog_park"]('+str(extent)[1:-1]+');     node["leisure"="playground"]('+str(extent)[1:-1]+');     node["amenity"="nightclub"]('+str(extent)[1:-1]+');     node["amenity"="restaurant"]('+str(extent)[1:-1]+');     node["amenity"="bar"]('+str(extent)[1:-1]+');     way["amenity"="cinema"]('+str(extent)[1:-1]+');     way["tourism"="zoo"]('+str(extent)[1:-1]+');     way["leisure"="swimming_pool"]('+str(extent)[1:-1]+');     way["leisure"="swimming_area"]('+str(extent)[1:-1]+');     way["leisure"="fitness_station"]('+str(extent)[1:-1]+');     way["amenity"="public_bath"]('+str(extent)[1:-1]+');     way["leisure"="dog_park"]('+str(extent)[1:-1]+');     way["leisure"="playground"]('+str(extent)[1:-1]+');     way["amenity"="nightclub"]('+str(extent)[1:-1]+');     way["amenity"="restaurant"]('+str(extent)[1:-1]+');     way["amenity"="bar"]('+str(extent)[1:-1]+');     relation["amenity"="cinema"]('+str(extent)[1:-1]+');     relation["tourism"="zoo"]('+str(extent)[1:-1]+');     relation["leisure"="swimming_pool"]('+str(extent)[1:-1]+');     relation["leisure"="swimming_area"]('+str(extent)[1:-1]+');     relation["leisure"="fitness_station"]('+str(extent)[1:-1]+');     relation["amenity"="public_bath"]('+str(extent)[1:-1]+');     relation["leisure"="dog_park"]('+str(extent)[1:-1]+');     relation["leisure"="playground"]('+str(extent)[1:-1]+');     relation["amenity"="nightclub"]('+str(extent)[1:-1]+');     relation["amenity"="restaurant"]('+str(extent)[1:-1]+');     relation["amenity"="bar"]('+str(extent)[1:-1]+'); ); (._;>;); out body;'

                checkDatasets = [supermarket_dataset_check,education_dataset_check, shopping_dataset_check, errands_dataset_check, leisure_dataset_check]
                datasetFalse = []
                datasetTrue = []
                for i in range(5):
                    if checkDatasets[i] == False:
                        datasetFalse.append(i)
                    else:
                        datasetTrue.append(i)

                Amenity_Cat = [Supermarket, Education,Shopping, OtherErrands, Leisure]


                st = 6
                for val in datasetFalse:
                    st += 4
                    results = Amenity_Cat[val]
                    osm = processing.run("native:filedownloader",
                                         {'URL': URL + results[2:],
                                          'OUTPUT': self.output + r'//tempDir//' + str(results[:2]) + '.geojson'})
                    # time.sleep(5)

                Amenity_Cat = ["SU", "ED", "SH", "OE", "LE"]

                for fac in Amenity_Cat:
                    Fac1 = processing.run("native:countpointsinpolygon",
                                          {'POLYGONS': walkareaAmenities,
                                           'POINTS': self.output+  r'//tempDir//'+ fac + '.geojson',
                                           'WEIGHT': '', 'CLASSFIELD': '', 'FIELD': fac + "_count",
                                           'OUTPUT': self.output +  r'//tempDir//'+ fac + '_count.csv'})['OUTPUT']

            def calculate_distance_point(infra_name, infra_count, infra_weight):
                values = []
                inputCSV = self.output +  r'//tempDir//'+ infra_name + '_count.csv'
                df = pd.read_csv(inputCSV)
                for i in range(featureCount):
                    name = infra_name
                    count = infra_count
                    val = infra_weight
                    new_Values = []
                    df_mask = df['Walk_ID'] == i+1
                    filtered_df = df[df_mask]
                    new_values = [(filtered_df.iloc[0, 6] - (filtered_df.iloc[1, 6])),
                                  (filtered_df.iloc[1, 6] - (filtered_df.iloc[2, 6])),
                                  (filtered_df.iloc[2, 6] - (filtered_df.iloc[3, 6])),
                                  (filtered_df.iloc[3, 6] - (filtered_df.iloc[4, 6])),
                                  (filtered_df.iloc[4, 6] - (filtered_df.iloc[5, 6])),
                                  (filtered_df.iloc[5, 6] - (filtered_df.iloc[6, 6])),
                                  (filtered_df.iloc[6, 6] - (filtered_df.iloc[7, 6])),
                                  (filtered_df.iloc[7, 6])]

                    filtered_df['new_values'] = new_values
                    filt2 = filtered_df.drop(
                        columns=['CENTER_LON', 'CENTER_LAT', 'AA_MODE', 'TOTAL_POP', infra_name + '_count'])
                    x = filt2.iloc[0, 2]

                    def check(x):
                        a = 3 if x > 2 else 2 if x == 2 else 1 if x == 1 else 0
                        return a

                    dist250 = check(filt2.iloc[7, 2])
                    dist500 = check(filt2.iloc[6, 2])
                    dist750 = check(filt2.iloc[5, 2])
                    dist1000 = check(filt2.iloc[4, 2])
                    dist1250 = check(filt2.iloc[3, 2])
                    dist1500 = check(filt2.iloc[2, 2])
                    dist1750 = check(filt2.iloc[1, 2])
                    dist2000 = check(filt2.iloc[0, 2])

                    liste = [dist250, dist500, dist750, dist1000, dist1250, dist1500, dist1750, dist2000]

                    l_no = 0
                    empty_list = []
                    for item in liste:
                        dist_values = [1, 0.9, 0.7, 0.4, 0.2, 0.05, 0.025, 0.005]
                        count = count - item
                        if count < 1:
                            empty_list.append((item + count) * dist_values[l_no])
                            break
                        else:
                            empty_list.append(item * dist_values[l_no])
                        l_no += 1

                    count_list = []

                    for i in range(8):
                        try:
                            count_list.append(int(round(empty_list[i], 1) / round(dist_values[i], 1)))
                        except:
                            count_list.append(0)

                    rawPos = []
                    for i in range(8):
                        if count_list[i] == 3.0:
                            rawPos.append(i + 1)
                            rawPos.append(i + 1)
                            rawPos.append(i + 1)
                        elif count_list[i] == 2.0:
                            rawPos.append(i + 1)
                            rawPos.append(i + 1)
                        elif count_list[i] == 1.0:
                            rawPos.append(i + 1)
                    L = []
                    weight = 0

                    for pos in rawPos:
                        L.append(float(dist_values[int(pos - 1)]) * val[weight])
                        weight += 1

                    values.append(round(sum(L), 1))

                return (values)


            downloadOsm()
            SU_point = calculate_distance_point("SU", self.SUPERMARKET_COUNT, [self.AMENITY_SUPERMARKET_WEIGHT_1, self.AMENITY_SUPERMARKET_WEIGHT_2, self.AMENITY_SUPERMARKET_WEIGHT_3])
            ED_point = calculate_distance_point("ED", self.EDUCATION_COUNT, [self.AMENITY_EDUCATION_WEIGHT_1,self.AMENITY_EDUCATION_WEIGHT_2,self.AMENITY_EDUCATION_WEIGHT_3])
            SH_point = calculate_distance_point("SH", self.SHOPPING_COUNT, [self.AMENITY_SHOPPING_WEIGHT_1, self.AMENITY_SHOPPING_WEIGHT_2, self.AMENITY_SHOPPING_WEIGHT_3])
            OE_point = calculate_distance_point("OE", self.OTHER_ERRANDS_COUNT, [self.AMENITY_OTHER_ERRANDS_WEIGHT_1, self.AMENITY_OTHER_ERRANDS_WEIGHT_2, self.AMENITY_OTHER_ERRANDS_WEIGHT_3])
            LE_point = calculate_distance_point("LE", self.LEISURE_COUNT, [self.AMENITY_LEISURE_WEIGHT_1,self.AMENITY_LEISURE_WEIGHT_2,self.AMENITY_LEISURE_WEIGHT_3])


            sum_list = zip(SU_point, ED_point, SH_point, OE_point,LE_point)
            maxVal=[]

            def checkMax(Val1, Val2):
                if Val1 == 3:
                    maxVal.append(Val2[0] + Val2[1] + Val2[2])
                elif Val1 == 2:
                    maxVal.append(Val2[0] + Val2[1])
                elif Val1 == 1:
                    maxVal.append(Val2[0])

            checkMax(self.SUPERMARKET_COUNT, [self.AMENITY_SUPERMARKET_WEIGHT_1, self.AMENITY_SUPERMARKET_WEIGHT_2, self.AMENITY_SUPERMARKET_WEIGHT_3])
            checkMax(self.EDUCATION_COUNT, [self.AMENITY_EDUCATION_WEIGHT_1,self.AMENITY_EDUCATION_WEIGHT_2,self.AMENITY_EDUCATION_WEIGHT_3])
            checkMax(self.SHOPPING_COUNT, [self.AMENITY_SHOPPING_WEIGHT_1, self.AMENITY_SHOPPING_WEIGHT_2, self.AMENITY_SHOPPING_WEIGHT_3])
            checkMax(self.OTHER_ERRANDS_COUNT, [self.AMENITY_OTHER_ERRANDS_WEIGHT_1, self.AMENITY_OTHER_ERRANDS_WEIGHT_2, self.AMENITY_OTHER_ERRANDS_WEIGHT_3])
            checkMax(self.LEISURE_COUNT, [self.AMENITY_LEISURE_WEIGHT_1,self.AMENITY_LEISURE_WEIGHT_2,self.AMENITY_LEISURE_WEIGHT_3])

            weighting = 100 / sum(maxVal)
            amenity_sum = [(a + b + c + d + e) * weighting for (a, b, c, d, e) in sum_list]

            def recalculateAmenities(oldval):
                if oldval >= 90:
                    return 10
                elif oldval >= 80 and oldval < 90:
                    return 9
                elif oldval >= 70 and oldval < 80:
                    return 8
                elif oldval >= 60 and oldval < 70:
                    return 7
                elif oldval >= 50 and oldval < 60:
                    return 6
                elif oldval >= 40 and oldval < 50:
                    return 5
                elif oldval >= 30 and oldval < 40:
                    return 4
                elif oldval >= 20 and oldval < 30:
                    return 3
                elif oldval >= 10 and oldval < 20:
                    return 2
                elif oldval >= 1 and oldval < 10:
                    return 1
                elif oldval == 0:
                    return 0

            amenity_rec = []
            for item in amenity_sum:
                amenity_rec.append(recalculateAmenities(item))

            featureCount = self.gridLayer.featureCount()
            iface.messageBar().pushMessage(str(featureCount))

            with open(grid_geojson) as d:
                data2 = json.load(d)
                for i in range(featureCount):
                    data2["features"][i]["properties"].update({"Amenities": amenity_sum[i]})
                    data2["features"][i]["properties"].update({"Amenities_points": amenity_rec[i]})
                json.dumps(data2)
                a = open(grid_geojson, "w")
                a.write((str(data2).replace("'", '"')).replace("NULL", "0").replace("None", "0"))
                a.close()

        def calculatePedshed():
            """function to calculate the pedestrian shed."""

            def recalculatePedShed(oldval):
                """Reclassify the pedshed values """
                try:
                    if oldval >= 90:
                        return 10
                    elif oldval >= 80 and oldval < 90:
                        return 9
                    elif oldval >= 70 and oldval < 80:
                        return 8
                    elif oldval >= 60 and oldval < 70:
                        return 7
                    elif oldval >= 50 and oldval < 60:
                        return 6
                    elif oldval >= 40 and oldval < 50:
                        return 5
                    elif oldval >= 30 and oldval < 40:
                        return 4
                    elif oldval >= 20 and oldval < 30:
                        return 3
                    elif oldval >= 10 and oldval < 20:
                        return 2
                    elif oldval < 10:
                        return 1
                except:
                    return 0

            radius = self.maxDist
            radius2 = int(radius) * int(radius)

            processing.run("qgis:fieldcalculator", {
                'INPUT': self.output + r'/tempDir/walkarea.geojson',
                'FIELD_NAME': 'PedShed', 'FIELD_TYPE': 0, 'FIELD_LENGTH': 10, 'FIELD_PRECISION': 3, 'NEW_FIELD': True,
                'FORMULA': '$area/(pi()*' + str(radius2) + ')*100',
                'OUTPUT': self.output + r'/tempDir/grid_neu.geojson'})
            processing.run("native:joinattributestable",
                           {'INPUT': grid_geojson, 'FIELD': 'Walk_ID',
                            'INPUT_2': self.output + r'/tempDir/grid_neu.geojson', 'FIELD_2': 'Walk_ID',
                            'FIELDS_TO_COPY': ['PedShed'], 'METHOD': 1, 'DISCARD_NONMATCHING': False, 'PREFIX': '',
                            'OUTPUT': self.output + r'/tempDir/pedshed.geojson'})

            #Put the values and the points in two different lists
            with open(self.output + r'/tempDir/pedshed.geojson') as d:
                pedshed = []
                pedshed_point = []
                data2 = json.load(d)
                for i in range(len(data2['features'])):
                    pedshed.append(data2["features"][i]["properties"]["PedShed"])
                for val in pedshed:
                    pedshed_point.append(recalculatePedShed(val))
                for i in range(len(data2['features'])):
                    data2["features"][i]["properties"].update({"PedShed_point": pedshed_point[i]})
                json.dumps(data2)
                a = open(self.output + r'/tempDir/pedshed.geojson', "w")
                a.write((str(data2).replace("'", '"')).replace("None", "null"))
                a.close()

            # Write the values from the lists in the 'main grid'
            with open(grid_geojson) as d:
                data2 = json.load(d)
                for i in range(len(data2['features'])):
                    data2["features"][i]["properties"].update({"PedShed": pedshed[i]})
                    data2["features"][i]["properties"].update({"PedShed_point": pedshed_point[i]})
                json.dumps(data2)
                a = open(grid_geojson, "w")
                a.write((str(data2).replace("'", '"')).replace("None", "null"))
                a.close()

        def calcGreen():
            """Function to calculate the percentage of green area in the walking radius """


            def getGreenOSM():
                with open(self.output + r'/tempDir/walkarea.geojson', 'r') as geojson:
                    xcoords = []
                    ycoords = []
                    data = json.load(geojson)
                    for f in data['features']:
                        geom = f['geometry']
                        for coord in geom['coordinates']:
                            if type(coord) == float:  # then its a point feature
                                xcoords.append(geom['coordinates'][0])
                                ycoords.append(geom['coordinates'][1])
                            elif type(coord) == list:
                                for c in coord:
                                    if type(c) == float:  # then its a linestring feature
                                        xcoords.append(coord[0])
                                        ycoords.append(coord[1])
                                    elif type(c) == list:  # then its a polygon feature
                                        xcoords.append(c[0])
                                        ycoords.append(c[1])
                extent = [round(min(ycoords), 2), round(min(xcoords), 2), round(max(ycoords), 2),
                          round(max(xcoords), 2)]
                URL = 'https://overpass.kumi.systems/api/interpreter?data='
                #Green ='[out:xml][timeout:25];(node["landuse"="meadow"]('+str(extent)[1:-1]+');node["landuse"="orchard"]('+str( extent)[1:-1]+');node["landuse"="allotments"]('+str(extent)[ 1:-1]+');node["landuse"="flowerbed"]('+str( extent)[1:-1]+');node["leisure"="garden"]('+str(extent)[ 1:-1]+');node["landuse"="grass"]('+str(extent)[ 1:-1]+');node["landuse"="village_green"]('+str( extent)[1:-1]+');node["leisure"="park"]('+str(extent)[ 1:-1]+');node["landuse"="recreation_ground"]('+str( extent)[1:-1]+');node["amenity"="grave_yard"]('+str(extent)[ 1:-1]+');node["landuse"="cemetery"]('+str( extent)[1:-1]+');node["cemetery"="sector"]('+str(extent)[ 1:-1]+');node["landuse"="greenfield"]('+str( extent)[1:-1]+');way["landuse"="meadow"]('+str(extent)[ 1:-1]+');way["landuse"="orchard"]('+str(extent)[ 1:-1]+');way["landuse"="allotments"]('+str( extent)[1:-1]+');way["landuse"="flowerbed"]('+str(extent)[ 1:-1]+');way["leisure"="garden"]('+str(extent)[ 1:-1]+');way["landuse"="grass"]('+str( extent)[1:-1]+');way["landuse"="village_green"]('+str(extent)[ 1:-1]+');way["leisure"="park"]('+str( extent)[1:-1]+');way["landuse"="recreation_ground"]('+str(extent)[ 1:-1]+');way["amenity"="grave_yard"]('+str( extent)[1:-1]+');way["landuse"="cemetery"]('+str(extent)[ 1:-1]+');way["cemetery"="sector"]('+str(extent)[ 1:-1]+');way["landuse"="greenfield"]('+str( extent)[1:-1]+');relation["landuse"="meadow"]('+str(extent)[ 1:-1]+');relation["landuse"="orchard"]('+str( extent)[1:-1]+');relation["landuse"="allotments"]('+str(extent)[ 1:-1]+');relation["landuse"="flowerbed"]('+str( extent)[1:-1]+');relation["leisure"="garden"]('+str(extent)[ 1:-1]+');relation["landuse"="grass"]('+str( extent)[1:-1]+');relation["landuse"="village_green"]('+str(extent)[ 1:-1]+');relation["leisure"="park"]('+str( extent)[1:-1]+');relation["landuse"="recreation_ground"]('+str(extent)[ 1:-1]+');relation["amenity"="grave_yard"]('+str( extent)[1:-1]+');relation["landuse"="cemetery"]('+str(extent)[ 1:-1]+');relation["cemetery"="sector"]('+str( extent)[1:-1]+');relation["landuse"="greenfield"]('+str(extent)[1:-1]+'););(._;>;);outbody;'
                #Green= '[out:xml][timeout:60];(node["landuse"="meadow"]('+str(extent)[1:-1]+');node["landuse"="grass"]('+str(extent)[1:-1]+');node["landuse"="village_green"]('+str(extent)[1:-1]+');node["leisure"="park"]('+str(extent)[1:-1]+');node["landuse"="recreation_ground"]('+str(extent)[1:-1]+');node["amenity"="grave_yard"]('+str(extent)[1:-1]+');node["landuse"="cemetery"]('+str(extent)[1:-1]+');node["landuse"="greenfield"]('+str(extent)[1:-1]+');way["landuse"="meadow"]('+str(extent)[1:-1]+');way["landuse"="grass"]('+str(extent)[1:-1]+');way["landuse"="village_green"]('+str(extent)[1:-1]+');way["leisure"="park"]('+str(extent)[1:-1]+');way["landuse"="recreation_ground"]('+str(extent)[1:-1]+');way["amenity"="grave_yard"]('+str(extent)[1:-1]+');way["landuse"="cemetery"]('+str(extent)[1:-1]+');way["landuse"="greenfield"]('+str(extent)[1:-1]+');relation["landuse"="meadow"]('+str(extent)[1:-1]+');relation["landuse"="grass"]('+str(extent)[1:-1]+');relation["landuse"="village_green"]('+str(extent)[1:-1]+');relation["leisure"="park"]('+str(extent)[1:-1]+');relation["landuse"="recreation_ground"]('+str(extent)[1:-1]+');relation["amenity"="grave_yard"]('+str(extent)[1:-1]+');relation["landuse"="cemetery"]('+str(extent)[1:-1]+');relation["landuse"="greenfield"]('+str(extent)[1:-1]+'););(._;>;);'
                Green = 'GR[out:xml] [timeout:90]; ( node["landuse"="meadow"](' + str(extent)[
                                                                                  1:-1] + '); node["landuse"="orchard"](' + str(
                    extent)[1:-1] + '); node["landuse"="allotments"](' + str(extent)[
                                                                         1:-1] + '); node["landuse"="flowerbed"](' + str(
                    extent)[1:-1] + '); node["leisure"="garden"](' + str(extent)[
                                                                     1:-1] + '); node["landuse"="grass"](' + str(
                    extent)[1:-1] + '); node["landuse"="village_green"](' + str(extent)[
                                                                            1:-1] + '); node["leisure"="park"](' + str(
                    extent)[1:-1] + '); node["landuse"="recreation_ground"](' + str(extent)[
                                                                                1:-1] + '); node["amenity"="grave_yard"](' + str(
                    extent)[1:-1] + '); node["landuse"="cemetery"](' + str(extent)[
                                                                       1:-1] + '); node["cemetery"="sector"](' + str(
                    extent)[1:-1] + '); node["landuse"="greenfield"](' + str(extent)[
                                                                         1:-1] + '); way["landuse"="meadow"](' + str(
                    extent)[1:-1] + '); way["landuse"="orchard"](' + str(extent)[
                                                                     1:-1] + '); way["landuse"="allotments"](' + str(
                    extent)[1:-1] + '); way["landuse"="flowerbed"](' + str(extent)[
                                                                       1:-1] + '); way["leisure"="garden"](' + str(
                    extent)[1:-1] + '); way["landuse"="grass"](' + str(extent)[
                                                                   1:-1] + '); way["landuse"="village_green"](' + str(
                    extent)[1:-1] + '); way["leisure"="park"](' + str(extent)[
                                                                  1:-1] + '); way["landuse"="recreation_ground"](' + str(
                    extent)[1:-1] + '); way["amenity"="grave_yard"](' + str(extent)[
                                                                        1:-1] + '); way["landuse"="cemetery"](' + str(
                    extent)[1:-1] + '); way["cemetery"="sector"](' + str(extent)[
                                                                     1:-1] + '); way["landuse"="greenfield"](' + str(
                    extent)[1:-1] + '); relation["landuse"="meadow"](' + str(extent)[
                                                                         1:-1] + '); relation["landuse"="orchard"](' + str(
                    extent)[1:-1] + '); relation["landuse"="allotments"](' + str(extent)[
                                                                             1:-1] + '); relation["landuse"="flowerbed"](' + str(
                    extent)[1:-1] + '); relation["leisure"="garden"](' + str(extent)[
                                                                         1:-1] + '); relation["landuse"="grass"](' + str(
                    extent)[1:-1] + '); relation["landuse"="village_green"](' + str(extent)[
                                                                                1:-1] + '); relation["leisure"="park"](' + str(
                    extent)[1:-1] + '); relation["landuse"="recreation_ground"](' + str(extent)[
                                                                                    1:-1] + '); relation["amenity"="grave_yard"](' + str(
                    extent)[1:-1] + '); relation["landuse"="cemetery"](' + str(extent)[
                                                                           1:-1] + '); relation["cemetery"="sector"](' + str(
                    extent)[1:-1] + '); relation["landuse"="greenfield"](' + str(extent)[
                                                                             1:-1] + '); ); (._;>;); out body; '


                greenArea = processing.run("native:filedownloader",
                                     {'URL': URL + Green[2:],
                                      'OUTPUT': self.output + r'//tempDir//greenarea_raw.geojson'})['OUTPUT']
                green_fixed = processing.run("native:fixgeometries", {
                    'INPUT': greenArea+'|layername=multipolygons',
                    'OUTPUT': self.output + r'//tempDir//greenarea.geojson'})['OUTPUT']


            getGreenOSM()



            def recalculateGreen(oldval):
                """Reclassify the greenarea values """
                try:
                    if oldval >= 22.5:
                        return 10
                    elif oldval >= 20 and oldval < 22.5:
                        return 9
                    elif oldval >= 17.5 and oldval < 20.0:
                        return 8
                    elif oldval >= 15 and oldval < 17.5:
                        return 7
                    elif oldval >= 12.5 and oldval < 15:
                        return 6
                    elif oldval >= 10.0 and oldval < 12.5:
                        return 5
                    elif oldval >= 7.5 and oldval < 10:
                        return 4
                    elif oldval >= 5 and oldval < 7.5:
                        return 3
                    elif oldval >= 2.5 and oldval < 5.0:
                        return 2
                    elif oldval < 2.5:
                        return 1
                except:
                    return 0

            processing.run("gdal:convertformat",
                           {'INPUT': self.output + r'//tempDir//greenarea.geojson', 'OPTIONS': '',
                            'OUTPUT': self.output + r'/tempDir/green.gpkg'})
            processing.run("gdal:convertformat",
                           {'INPUT': self.output + r'/tempDir/walkarea.geojson', 'OPTIONS': '',
                            'OUTPUT': self.output + r'/tempDir/walkarea.gpkg'})

            processing.run("native:calculatevectoroverlaps",
                           {'INPUT': self.output + r'/tempDir/walkarea.gpkg',
                            'LAYERS': [self.output + r'/tempDir/green.gpkg'],
                            'OUTPUT': self.output + r'/tempDir/Green_raw.geojson'})
            processing.run("native:joinattributestable",
                           {'INPUT': self.output + r'/tempDir/pedshed.geojson', 'FIELD': 'Walk_ID',
                            'INPUT_2': self.output + r'/tempDir/Green_raw.geojson', 'FIELD_2': 'Walk_ID',
                            'FIELDS_TO_COPY': ['green_pc'], 'METHOD': 1, 'DISCARD_NONMATCHING': False, 'PREFIX': '',
                            'OUTPUT': self.output + r'/tempDir/Green_Score.geojson'})

            # Put the values and the points in two different lists
            with open(self.output + r'/tempDir/Green_Score.geojson') as d:
                green = []
                green_point = []
                data2 = json.load(d)
                for i in range(len(data2['features'])):
                    green.append(data2["features"][i]["properties"]["green_pc"])
                for val in green:
                    green_point.append(recalculateGreen(val))
                json.dumps(data2)
                with open(self.output + r'/tempDir/Green_Score.geojson', "w") as a:
                    a.write((str(data2).replace("'", '"')).replace("None", "null"))
                #a.close()

            # Write the values from the lists in the 'main grid'
            with open(grid_geojson) as d:
                data2 = json.load(d)
                for i in range(len(data2['features'])):
                    data2["features"][i]["properties"].update({"Green": green[i]})
                    data2["features"][i]["properties"].update({"Green_point": green_point[i]})
                json.dumps(data2)
                with open(grid_geojson, "w") as a:
                    a.write((str(data2).replace("'", '"')).replace("None", "null"))
                #a.close()



        def calc():
            """Calculate the OS-Walk-EU Index from the indicators. Multiplying the summed values with correction factor set the value range 0 - 100 """
            AMENITIES_WEIGHT = self.AMENITIES_WEIGHT
            PEDESTRIAN_SHED_WEIGHT = self.PEDESTRIAN_SHED_WEIGHT
            GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT = self.GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT
            cor_fac = 100 / ((
                                     PEDESTRIAN_SHED_WEIGHT + GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT + AMENITIES_WEIGHT) * 10)

            if elevation_global ==True:
                temp_result = processing.run("native:deletecolumn", {
                        'INPUT':self.output + r'/tempDir/slope_tempFile2.geojson',
                        'COLUMN':['Walk_ID_2','CENTER_LON','CENTER_LAT','AA_METERS','AA_MODE','TOTAL_POP'],
                        'OUTPUT':self.output + r'/tempDir/result_tempFile3.geojson'})['OUTPUT']

                processing.run("qgis:advancedpythonfieldcalculator",
                                   {'INPUT': temp_result,
                                    'FIELD_NAME': 'Result',
                                    'FIELD_TYPE': 1,
                                    'FIELD_LENGTH': 10,
                                    'FIELD_PRECISION': 3,
                                    'GLOBAL': 'AMENITIES_WEIGHT =' + str(self.AMENITIES_WEIGHT) +
                                              "; PEDESTRIAN_SHED_WEIGHT=" + str(PEDESTRIAN_SHED_WEIGHT) +
                                              "; GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT=" + str(
                                        GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT) +
                                              "; cor_fac=" + str(cor_fac),
                                    'FORMULA': 'value = ((<Amenities_points> *AMENITIES_WEIGHT+<Green_point> *GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT+<PedShed_point>*PEDESTRIAN_SHED_WEIGHT)*cor_fac)-((<Amenities_points> *AMENITIES_WEIGHT+<Green_point> *GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT+<PedShed_point>*PEDESTRIAN_SHED_WEIGHT)*cor_fac*(<Slope_red>/100))',
                                    'OUTPUT': self.output + r'/tempDir/WalkEU.geojson'})

                processing.run("gdal:convertformat",
                                   {'INPUT': self.output + r'/tempDir/WalkEU.geojson', 'OPTIONS': '',
                                    'OUTPUT': self.output + r'/WalkEU.gpkg'})

                processing.run("gdal:convertformat",
                                   {'INPUT': self.output + r'/tempDir/walkarea.geojson', 'OPTIONS': '',
                                    'OUTPUT': self.output + r'/walkarea.gpkg'})

            else:
                processing.run("qgis:advancedpythonfieldcalculator",
                               {'INPUT': grid_geojson,
                                'FIELD_NAME': 'Result',
                                'FIELD_TYPE': 1,
                                'FIELD_LENGTH': 10,
                                'FIELD_PRECISION': 3,
                                'GLOBAL': 'AMENITIES_WEIGHT =' + str(self.AMENITIES_WEIGHT) +
                                          "; PEDESTRIAN_SHED_WEIGHT=" + str(PEDESTRIAN_SHED_WEIGHT) +
                                          "; GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT=" + str(
                                    GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT) +
                                          "; cor_fac=" + str(cor_fac),
                                'FORMULA': 'value = (<Amenities_points> *AMENITIES_WEIGHT+<Green_point> *GREEN_AND_BLUE_INFRASTRUCTURE_WEIGHT+<PedShed_point>*PEDESTRIAN_SHED_WEIGHT)*cor_fac',
                                'OUTPUT': self.output + r'/tempDir/WalkEU.geojson'})

                processing.run("gdal:convertformat",
                               {'INPUT': self.output + r'/tempDir/WalkEU.geojson', 'OPTIONS': '',
                                'OUTPUT': self.output + r'/WalkEU.gpkg'})

                processing.run("gdal:convertformat",
                               {'INPUT': self.output + r'/tempDir/walkarea.geojson', 'OPTIONS': '',
                                'OUTPUT': self.output + r'/walkarea.gpkg'})



        # these two functions are run in parallel in QgsTasks. Uncomment of QgsTasks crashes Qgis more than twice.
        self.dlg = WalkabilityToolDialog()
        self.dlg.progressBar.setValue(25)
        calculateAmenities()


        # rest of the functions which run sequentially
        self.dlg.progressBar.setValue(50)

        calculatePedshed()
        calcGreen()


        self.dlg.progressBar.setValue(75)
        checkSlope()
        calc()

        #shutil.rmtree(self.output + r'/tempDir', ignore_errors=True)
        self.dlg.progressBar.setValue(100)
        return {}

        # ---->Adding the final walkability score into the QGIS instance

    # Delete Temporary Directory
    def delete_temp_dir(self):
        """ Delete tempDir and all its contents."""
        tempDir = self.output + r'/tempDir'
        """
        try:
            shutil.rmtree(tempDir)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))
        """
    # Upload walkability result on the iface
    def upload_walkability_result(self):
        walkability_gpkg = self.output + r'/WalkEU.gpkg'
        gpkg_layers = [layer.GetName() for layer in ogr.Open(walkability_gpkg)]
        walkability_layer = 'WalkEU'
        if walkability_layer in gpkg_layers:
            iface.addVectorLayer(walkability_gpkg + "|layername=" + walkability_layer, walkability_layer, 'ogr')

        else:
            print('Error: there is no layer named "{}" '.format(walkability_layer))
