
from qgis.PyQt.QtCore import QCoreApplication


def tr(string):
    """
    Returns a translatable string with the self.tr() function.
    """
    return QCoreApplication.translate('Processing', string)





def shortHelpString():
    """
    Returns a localised short helper string for the algorithm. This string
    should provide a basic description about what the algorithm does and the
    parameters and outputs associated with it..
    """
    return tr("This tool calculates the walkability of urban neighborhoods "
              "for 500 by 500 meter vector grids. You can either create this grid for "
              "your study area using the Create Grid tool in QGIS or download such a "
              "grid from https://inspire-geoportal.ec.europa.eu if you want it to "
              "conform to EU INSPIRE requirements.\n\n"
              "The tool requires that you have assigned population data to this grid "
              "from an input population source. Ideally you would use block data for "
              "your study area if available, and intersect and dissolve it with the "
              "input grid. Alternatively you can downloaded a 1x1 km population grid "
              "for your study area from https://ghsl.jrc.ec.europa.eu/download.php?ds=pop. "
              "In this case you will have to disaggregate 1x1 km data to the 500x500 meter "
              "input grid. We recommend to use a split factor according to the share of "
              "residential land use or the number of residential households/buildings in this case. "
              "This requires expertise in rule-based disaggregation procedures.\n\n"
              "Please specify a polygon layer that covers all recreational areas in your "
              "study area. It can be sourced from your own local data or from "
              "https://land.copernicus.eu/local/urban-atlas, from which you would need "
              "to extract green and water areas.\n\n"
              "Amenities are facilities, attractions or services that attract walking trips. "
              "Please provide a point layer with the attribute POI_ID filled with numeric codes "
              "for the following categories: 1 (retail), 2 (entertainment), 3 (food-related), "
              "4 (civic and institutional), 5 (office), 6 (recreation). A potential data source "
              "for this is OpenStreetMap for example from www.geofabrike.de.\n\n The maximum walking "
              "distance along the street network is used to search for such amenities based "
              "on an origin located at the center of each grid cell. The script associated "
              "to this tool contains constants to define the number of amenities per category "
              "to be searched for and their weight in the algorithm (default: one amenity in "
              "each category and equal weighting). These settings can manually be changed in "
              "the script to suit your requirements.\n\n"
              "The provision of elevation data is optional. If your study area has "
              "considerable slopes that affect walkability we recommend to use a digital "
              "elevation model as input. The data can be sourced from your own local "
              "data or from https://land.copernicus.eu/imagery-in-situ/eu-dem/eu-dem-v1.1.\n\n"
              "If you specify a slope threshold value and associated slope shrinkage "
              "factor the tool reduces the maximum walking distance with a percentage "
              " calculated from: [Maximum walking distance] = [Maximum walking distance] - "
              "[Actual slope value from input] - ([Slope threshold] * [Slope shrinkage factor]).\n\n"
              "Additional information is available at https://gitlab.com/os-walk-eu-group/os-walk-eu."
              )
